package com.example.souma.easylifeforteacher;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.model.beans.Event;
import com.example.souma.easylifeforteacher.model.db.ClasseDAO;
import com.example.souma.easylifeforteacher.model.db.EventDAO;
import com.example.souma.easylifeforteacher.view.EventAdapter;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by souma on 7/15/2018.
 */

public class EventTab extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, EventAdapter.OnEventListener {

    View v;
    RecyclerView rv_event;
    Button bt_archive_event, bt_future_events;
    FloatingActionButton bt_new_event;

    private String event_description;

    //Données
    private ArrayList<Event> eventArrayList;

    //Outils
    EventAdapter eventAdapter;
    private Calendar calendar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.event_layout, container, false);


        bt_new_event = (FloatingActionButton) v.findViewById(R.id.bt_new_event);
        bt_archive_event = (Button) v.findViewById(R.id.bt_archive_event);
        bt_future_events = (Button) v.findViewById(R.id.bt_future_events);
        rv_event = (RecyclerView) v.findViewById(R.id.rv_event);

        calendar = Calendar.getInstance();
        eventArrayList = EventDAO.getEvents();
        eventAdapter = new EventAdapter(eventArrayList, this);

        rv_event.setAdapter(eventAdapter);
        rv_event.setLayoutManager(new LinearLayoutManager(this.getContext()));

        bt_new_event.setOnClickListener(this);
        bt_archive_event.setOnClickListener(this);
        bt_future_events.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        if (v == bt_new_event) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            //Afficher le dialog
            datePickerDialog.show();
        } else if (v == bt_archive_event) {
            //NotificationUtils.sendNotification(EventTab.this.getContext(), "Mon message");
            NotificationUtils.programmeNotification(EventTab.this.getContext(), "Mon message", 0);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(year, month, dayOfMonth);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this.getContext(), this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        askDescriptionDialog();
    }

    public void askDescriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle("Description");
        final EditText input = new EditText(this.getContext());
        builder.setView(input);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                event_description = input.getText().toString();
                askClasseDialog();
            }
        });

        builder.setNegativeButton(R.string.cancel, null);

        builder.show();
    }

    public void askClasseDialog() {
        final ArrayList<String> classeListe = ClasseDAO.getStringClasses();

        // convertir la liste en un tableau de string
        final String[] tableau = classeListe.toArray(new String[classeListe.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.classe_name_for_event)
                .setItems(tableau, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        //Créer une donnée
                        Event event = new Event(calendar.getTime(), event_description, tableau[which]);

                        //Inserer dans la base de données
                        if (EventDAO.insert(event)) {
                            //Ajouter ma donnée dans la liste
                            eventArrayList.add(event);
                            eventAdapter.notifyDataSetChanged();
                            eventAdapter.clearAll();

                        } else {
                            Toast.makeText(EventTab.this.getContext(), "Non ajouté", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
        builder.show();
    }

    @Override
    public void onclick(final Event event) {
        PopupMenu popup = new PopupMenu(EventTab.this.getContext(), v.findViewById(R.id.bt_options_event));
        //inflating menu from xml resource
        popup.inflate(R.menu.options_menu_event);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        //handle menu2 click
                        askDeleteConfirmationDialog(event);
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    //confirmer la suppression
    public void askDeleteConfirmationDialog(final Event event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.ask_event_delete_confirmation));

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EventDAO.deleteOne(event);
                eventArrayList.remove(event);
                eventAdapter.clearAll();
                eventAdapter.notifyDataSetChanged();
                Toast.makeText(EventTab.this.getContext(), R.string.event_deleted, Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton(getString(R.string.no), null);

        builder.show();
    }

    @Override
    public void onPause() {
        super.onPause();

        eventAdapter.clearAll();
    }

    @Override
    public void setInitialSavedState(SavedState state) {
        super.setInitialSavedState(state);
        eventAdapter.clearAll();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        eventAdapter.clearAll();

    }
}
