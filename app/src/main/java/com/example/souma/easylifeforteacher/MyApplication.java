package com.example.souma.easylifeforteacher;

import android.app.Application;

import com.example.souma.easylifeforteacher.model.db.MySQLiteDatabase;
import com.facebook.stetho.Stetho;

/**
 * Created by souma on 8/24/2018.
 */

public class MyApplication extends Application {

    private static MySQLiteDatabase mySQLiteDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);

        mySQLiteDatabase = new MySQLiteDatabase(this);
    }

    public static MySQLiteDatabase getMySQLiteDatabase() {
        return mySQLiteDatabase;
    }
}
