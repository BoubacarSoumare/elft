package com.example.souma.easylifeforteacher.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.R;
import com.example.souma.easylifeforteacher.SharedPreferencesUtils;
import com.example.souma.easylifeforteacher.model.beans.User;

public class ConnectionActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_username, et_password;
    Button bt_connexion, bt_inscription;
    CheckBox cb_remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        bt_connexion = (Button) findViewById(R.id.bt_connexion);
        bt_inscription = (Button) findViewById(R.id.bt_inscription);

        bt_connexion.setOnClickListener(this);
        bt_inscription.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == bt_connexion) {
            User user = SharedPreferencesUtils.loadUser(this);
            if (user != null) {
                if (et_username.getText().toString().equals(user.getUsername()) && et_password.getText().toString().equals(user.getPassword())) {
                    Intent intent = new Intent(ConnectionActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Nom d'utilisateut ou mot de passe incorrect !", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Nom d'utilisateut ou mot de passe incorrect !", Toast.LENGTH_SHORT).show();
            }
        } else if (v == bt_inscription) {
            Intent intent = new Intent(ConnectionActivity.this, InscriptionActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        et_username.setText("");
        et_password.setText("");
    }
}
