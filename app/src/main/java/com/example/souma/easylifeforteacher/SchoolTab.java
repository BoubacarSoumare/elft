package com.example.souma.easylifeforteacher;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.model.beans.School;
import com.example.souma.easylifeforteacher.model.db.SchoolDAO;
import com.example.souma.easylifeforteacher.view.SchoolAdapter;

import java.util.ArrayList;

/**
 * Created by souma on 7/15/2018.
 */
public class SchoolTab extends Fragment implements View.OnClickListener, SchoolAdapter.OnSchoolListener {

    View v;
    Button bt_manage_schools, bt_new_school;
    RecyclerView rv_school;
    private String initials, name_school;

    //Données
    ArrayList<School> schools;

    //Outils
    private SchoolAdapter schoolAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.school_layout, container, false);

        bt_manage_schools = (Button) v.findViewById(R.id.bt_manage_schools);
        bt_new_school = (Button) v.findViewById(R.id.bt_new_school);
        rv_school = (RecyclerView) v.findViewById(R.id.rv_school);

        bt_manage_schools.setOnClickListener(this);
        bt_new_school.setOnClickListener(this);

        schools = SchoolDAO.getSchools();

        schoolAdapter = new SchoolAdapter(schools, this);

        rv_school.setAdapter(schoolAdapter);
        rv_school.setLayoutManager(new LinearLayoutManager(this.getContext()));

        return v;
    }

    @Override
    public void onClick(View v) {
        if (v == bt_manage_schools) {
            Toast.makeText(getActivity(), "Gerer mes ecoles", Toast.LENGTH_SHORT).show();
        } else if (v == bt_new_school) {
            //Toast.makeText(getActivity(), "Nouvelle ecole", Toast.LENGTH_SHORT).show();
            askInitialsDialog();
        }
    }

    //Boite de dialog pour demander le sigle
    public void askInitialsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.initials));
        final EditText input = new EditText(this.getContext());
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                initials = input.getText().toString();

                askNameDialog();
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), null);

        builder.show();
    }

    //Boite de dialog pour demander le nom
    public void askNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.name_school));
        final EditText input = new EditText(this.getContext());
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name_school = input.getText().toString();

                //Créer une donnée
                School school = new School(initials, name_school);

                //Inserer dans la base de données
                if (SchoolDAO.insert(school)) {
                    //Ajouter ma donnée dans la liste
                    schools.add(school);
                    schoolAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(SchoolTab.this.getContext(), "Non ajouté", Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), null);

        builder.show();
    }

    @Override
    public void onclick(final School school) {
        /*
        SchoolDAO.deleteAll();
        schools.remove(school);
        schoolAdapter.notifyDataSetChanged();*/

        //creating a popup menu
        PopupMenu popup = new PopupMenu(SchoolTab.this.getContext(), v.findViewById(R.id.bt_options_school));
        //inflating menu from xml resource
        popup.inflate(R.menu.options_menu_school);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.schedule:
                        //handle menu1 click
                        break;
                    case R.id.delete:
                        //handle menu2 click
                        askDeleteConfirmationDialog(school);
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    //confirmer la suppression
    public void askDeleteConfirmationDialog(final School school) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.ask_school_delete_confirmation));

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SchoolDAO.deleteOne(school);
                schools.remove(school);
                schoolAdapter.notifyDataSetChanged();
                Toast.makeText(SchoolTab.this.getContext(), R.string.school_deleted, Toast.LENGTH_SHORT).show();

            }
        });

        builder.setNegativeButton(getString(R.string.no), null);

        builder.show();
    }
}
