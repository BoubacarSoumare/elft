package com.example.souma.easylifeforteacher.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by souma on 8/23/2018.
 */

public class MySQLiteDatabase extends SQLiteOpenHelper {

    public static final String DB_NAME = "mydb.db";
    public static final int DB_VERSION = 3;

    public MySQLiteDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SchoolDAO.SQL_CREATION_TABLE);
        db.execSQL(ClasseDAO.SQL_CREATION_TABLE);
        db.execSQL(EventDAO.SQL_CREATION_TABLE);

        //create of a sample school
        db.execSQL("INSERT INTO School (Initials, Name)VALUES ('TE', 'TestEcole');");
        //create of a sample class
        db.execSQL("INSERT INTO Classe (NameClasse, Matiere, ID_School, NameSchool)VALUES ('TestClass', 'TestMatière', 'TE', 'TestEcole');");

        Date now = new Date();
        //create of a sample event
        db.execSQL("INSERT INTO Event (Date, Description, NameClasse, NameMATIERE, NameSchool)VALUES ("+addDay(now)+", 'Devoir', 'TestClass', 'TestMatière', 'TestEcole');");
    }

    public static Long addDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, 7);
        return cal.getTime().getTime();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (newVersion < 10) {
            db.execSQL("DROP TABLE " + SchoolDAO.TABLE_NAME + ";");
            db.execSQL("DROP TABLE " + ClasseDAO.TABLE_NAME + ";");
            db.execSQL("DROP TABLE " + EventDAO.TABLE_NAME + ";");
            onCreate(db);
        }
    }


}
