package com.example.souma.easylifeforteacher;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.souma.easylifeforteacher.model.beans.School;
import com.example.souma.easylifeforteacher.model.beans.User;
import com.google.gson.Gson;

/**
 * Created by souma on 7/16/2018.
 */

public class SharedPreferencesUtils {

    private final static String MY_FILE = "MyFile.xml";
    private final static String USER_KEY = "USER_KEY";
    private final static String SCHOOL_KEY = "SCHOOL_KEY";
    private final static Gson gson = new Gson();

    public static void saveUser(Context context, User user) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = gson.toJson(user);
        editor.putString(USER_KEY, json);
        editor.apply();
    }

    public static User loadUser(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_FILE, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(USER_KEY, null);
        if (json != null) {
            return gson.fromJson(json, User.class);
        } else {
            return null;
        }
    }

    public static void saveSchool(Context context, School school) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = gson.toJson(school);
        editor.putString(SCHOOL_KEY, json);
        editor.apply();
    }

    public static School loadSchool(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_FILE, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(SCHOOL_KEY, null);
        if (json != null) {
            return gson.fromJson(json, School.class);
        } else {
            return null;
        }
    }
}
