package com.example.souma.easylifeforteacher.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.souma.easylifeforteacher.R;
import com.example.souma.easylifeforteacher.model.beans.Classe;

import java.util.ArrayList;

/**
 * Created by souma on 8/21/2018.
 */

public class ClasseAdapter extends RecyclerView.Adapter<ClasseAdapter.ViewHolder> {

    private ArrayList<Classe> classes;
    private OnClasseListener onClasseListener;

    public ClasseAdapter(ArrayList<Classe> classes, OnClasseListener onClasseListener) {
        this.classes = classes;
        this.onClasseListener = onClasseListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_classe, parent, false);

        return new ClasseAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Classe classe = classes.get(position);

        holder.tv_classe_name.setText(classe.getName_classe().toUpperCase() + " (" + classe.getMatiere().toLowerCase() + ")");
        holder.tv_school.setText(classe.getName_school());

        holder.bt_options_classe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClasseListener != null) {
                    onClasseListener.onclick(classe);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return classes.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_classe_name, tv_school;
        public View bt_options_classe;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_classe_name = (TextView) itemView.findViewById(R.id.tv_name_classe);
            tv_school = (TextView) itemView.findViewById(R.id.tv_school);
            bt_options_classe = itemView.findViewById(R.id.bt_options_classe);
        }
    }

    // Notre interface de communication pour le callback
    public interface OnClasseListener {
        void onclick(Classe classe);
    }
}
