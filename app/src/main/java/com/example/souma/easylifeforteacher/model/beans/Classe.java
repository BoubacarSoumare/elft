package com.example.souma.easylifeforteacher.model.beans;

/**
 * Created by souma on 8/22/2018.
 */

public class Classe {

    private String name_classe, matiere, name_school;
    private Long id;
    private int id_school;

    public String getName_classe() {
        return name_classe;
    }

    public void setName_classe(String name_classe) {
        this.name_classe = name_classe;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getId_school() {
        return id_school;
    }

    public void setId_school(int id_school) {
        this.id_school = id_school;
    }

    public Classe(String name_classe, String matiere, int id_school, String name_school) {
        this.name_classe = name_classe;
        this.matiere = matiere;
        this.id_school = id_school;
        this.name_school = name_school;
    }

    public String getName_school() {
        return name_school;
    }

    public void setName_school(String name_school) {
        this.name_school = name_school;
    }

    public Classe(String name_classe, String matiere, String name_school) {
        this.name_classe = name_classe;
        this.matiere = matiere;
        this.name_school = name_school;
    }

}
