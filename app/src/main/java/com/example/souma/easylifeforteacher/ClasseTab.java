package com.example.souma.easylifeforteacher;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.model.beans.Classe;
import com.example.souma.easylifeforteacher.model.db.ClasseDAO;
import com.example.souma.easylifeforteacher.model.db.SchoolDAO;
import com.example.souma.easylifeforteacher.view.ClasseAdapter;

import java.util.ArrayList;

/**
 * Created by souma on 7/15/2018.
 */

public class ClasseTab extends Fragment implements View.OnClickListener, ClasseAdapter.OnClasseListener {

    View v;
    Button bt_new_classe, bt_manage_classe;
    RecyclerView rv_classe;
    private String name_classe, matiere, school;

    //Données
    ArrayList<Classe> classes;

    //Outils
    private ClasseAdapter classeAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.classe_layout, container, false);

        bt_new_classe = (Button) v.findViewById(R.id.bt_new_classe);
        bt_manage_classe = (Button) v.findViewById(R.id.bt_manage_classe);
        rv_classe = (RecyclerView) v.findViewById(R.id.rv_classe);

        bt_new_classe.setOnClickListener(this);
        bt_manage_classe.setOnClickListener(this);

        classes = ClasseDAO.getClasses();

        classeAdapter = new ClasseAdapter(classes, this);

        rv_classe.setAdapter(classeAdapter);
        rv_classe.setLayoutManager(new LinearLayoutManager(this.getContext()));

        return v;
    }

    @Override
    public void onClick(View v) {
        if (v == bt_new_classe) {
            askClasseNameDialog();
        } else if (v == bt_manage_classe) {
            Toast.makeText(getActivity(), "Gerer mes classe", Toast.LENGTH_SHORT).show();
        }
    }

    //Boite de dialog pour demander le nom de la classe
    public void askClasseNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.classe_name));
        final EditText input = new EditText(this.getContext());
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name_classe = input.getText().toString();

                askMatiereDialog();
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), null);

        builder.show();
    }

    //Boite de dialog pour demander la matiere de la classe
    public void askMatiereDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.ask_matiere));
        final EditText input = new EditText(this.getContext());
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                matiere = input.getText().toString();

                askSchoolDialog();
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), null);

        builder.show();
    }

    //Boite de dialog pour demander l'ecole dans la quelle appartient la classe
    public void askSchoolDialog() {
        final ArrayList<String> scoolListe = SchoolDAO.getStringSchools();

        // convertir la liste en un tableau de string
        final String[] tableau = scoolListe.toArray(new String[scoolListe.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.school_name_for_classe)
                .setItems(tableau, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        //Créer une donnée
                        Classe classe = new Classe(name_classe, matiere, which + 1, tableau[which]);

                        //Inserer dans la base de données
                        if (ClasseDAO.insert(classe)) {
                            //Ajouter ma donnée dans la liste
                            classes.add(classe);
                            classeAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(ClasseTab.this.getContext(), "Non ajouté", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        builder.show();
    }


    @Override
    public void onclick(final Classe classe) {
//creating a popup menu
        PopupMenu popup = new PopupMenu(ClasseTab.this.getContext(), v.findViewById(R.id.bt_options_classe));
        //inflating menu from xml resource
        popup.inflate(R.menu.options_menu_classe);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.students:
                        //handle menu1 click
                        break;
                    case R.id.delete:
                        //handle menu2 click
                        askDeleteConfirmationDialog(classe);
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    //confirmer la suppression
    public void askDeleteConfirmationDialog(final Classe classe) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());

        builder.setTitle(getString(R.string.ask_classe_delete_confirmation));

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClasseDAO.deleteOne(classe);
                classes.remove(classe);
                classeAdapter.notifyDataSetChanged();
                Toast.makeText(ClasseTab.this.getContext(), R.string.class_deleted, Toast.LENGTH_SHORT).show();

            }
        });

        builder.setNegativeButton(getString(R.string.no), null);

        builder.show();
    }
}
