package com.example.souma.easylifeforteacher.model.beans;

/**
 * Created by souma on 8/22/2018.
 */

public class School {

    private String initials, name;
    private Long id;

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public School(String initials, String name) {
        this.initials = initials;
        this.name = name;
    }

    @Override
    public String toString() {
        return "School{" +
                "initials='" + initials + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
