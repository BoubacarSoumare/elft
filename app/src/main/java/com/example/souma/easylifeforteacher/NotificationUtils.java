package com.example.souma.easylifeforteacher;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.example.souma.easylifeforteacher.controller.MainActivity;

/**
 * Created by souma on 8/29/2018.
 */

public class NotificationUtils {

    public static void sendNotification(Context context, String message) {

        Notification notification = createNotification(context, message);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(1, notification);
    }

    private static Notification createNotification(Context context, String message) {

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle("Préparez vous !")
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL);

        return builder.build();
    }

    public static void programmeNotification(Context context, String message, long delayInMillis) {

        Log.w("Tag", "Delay : " + delayInMillis);

        Notification notification = createNotification(context, message);

        Intent notifiIntent = new Intent(context, NotificationReceiver.class);
        notifiIntent.putExtra(NotificationReceiver.NOTIFICATION_EXTRA, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notifiIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delayInMillis;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);

    }
}
