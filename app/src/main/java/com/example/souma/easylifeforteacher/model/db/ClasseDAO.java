package com.example.souma.easylifeforteacher.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.souma.easylifeforteacher.MyApplication;
import com.example.souma.easylifeforteacher.model.beans.Classe;

import java.util.ArrayList;

/**
 * Created by souma on 8/25/2018.
 */

public class ClasseDAO {

    public static final String TABLE_NAME = "Classe";
    private static final String COL_CLASSE_NAME = "NameClasse";
    private static final String COL_MATIERE = "Matiere";
    private static final String COL_ID_SCHOOL = "ID_School";
    private static final String COL_NAME_SCHOOL = "NameSchool";
    public static final String COL_ID = "ID";

    public static final String SQL_CREATION_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_CLASSE_NAME + " TEXT,"
            + COL_MATIERE + " TEXT,"
            + COL_ID_SCHOOL + " INTEGER,"
            + COL_NAME_SCHOOL + " TEXT,"
            + " FOREIGN KEY" + "(" + COL_ID_SCHOOL + ") REFERENCES " + SchoolDAO.TABLE_NAME + "(" + SchoolDAO.COL_ID + "));";

    public static boolean insert(Classe classe) {
        //On ouvre la base en ecriture
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        //Crétaion d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //On lui ajouter une valeur assoiée a une clé qui est le nom de la colonne
        values.put(COL_CLASSE_NAME, classe.getName_classe());
        values.put(COL_MATIERE, classe.getMatiere());
        values.put(COL_ID_SCHOOL, classe.getId_school());
        values.put(COL_NAME_SCHOOL, classe.getName_school());

        classe.setId(db.insert(TABLE_NAME, null, values));

        db.close();

        if (classe.getId() <= 0) {
            //Gestion de l'erreur
            Log.w("TAG_SQL", "Insertion echoué");
            return false;
        }
        return true;
    }

    public static int update(Classe classe) {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COL_CLASSE_NAME, classe.getName_classe());
        values.put(COL_MATIERE, classe.getMatiere());
        values.put(COL_ID_SCHOOL, classe.getId_school());
        values.put(COL_NAME_SCHOOL, classe.getName_school());

        int nb = db.update(TABLE_NAME, values, COL_ID + " = " + classe.getId(), null);
        db.close();
        return nb;
    }

    public static int deleteAll() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        int nb = db.delete(TABLE_NAME, null, null);
        db.close();
        return nb;
    }

    public static int deleteOne(Classe classe) {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        int nb = db.delete(TABLE_NAME, COL_ID + " = " + classe.getId(), null);
        db.close();
        return nb;
    }

    public static ArrayList<Classe> getClasses() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_CLASSE_NAME, COL_MATIERE, COL_ID_SCHOOL, COL_NAME_SCHOOL}, null, null, null, null, null);
        ArrayList<Classe> arrayList = cursorToClasses(cursor);
        db.close();

        return arrayList;
    }

    /**
     * Une methode qui convertit un cursor en liste de schools
     */
    private static ArrayList<Classe> cursorToClasses(Cursor c) {
        ArrayList<Classe> classeListe = new ArrayList<>();

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    String classe_name = c.getString(c.getColumnIndex(COL_CLASSE_NAME));
                    String matiere = c.getString(c.getColumnIndex(COL_MATIERE));
                    String name_school = c.getString(c.getColumnIndex(COL_NAME_SCHOOL));
                    Long id = c.getLong(c.getColumnIndex(COL_ID));
                    Long id_school = c.getLong(c.getColumnIndex(COL_ID_SCHOOL));

                    Classe classe = new Classe(classe_name, matiere, name_school);
                    classe.setId(id);

                    classeListe.add(classe);
                } while (c.moveToNext());
            }
        }

        c.close();
        return classeListe;
    }

    public static ArrayList<String> getStringClasses() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_CLASSE_NAME, COL_MATIERE, COL_NAME_SCHOOL}, null, null, null, null, null);
        ArrayList<String> arrayList = cursorToStringClasses(cursor);
        db.close();

        return arrayList;
    }

    private static ArrayList<String> cursorToStringClasses(Cursor c) {
        ArrayList<String> classeListe = new ArrayList<>();

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    String classe_name = c.getString(c.getColumnIndex(COL_CLASSE_NAME));
                    String matiere = c.getString(c.getColumnIndex(COL_MATIERE));
                    String name_school = c.getString(c.getColumnIndex(COL_NAME_SCHOOL));
                    Long id = c.getLong(c.getColumnIndex(COL_ID));

                    Classe classe = new Classe(classe_name, matiere, name_school);
                    classe.setId(id);

                    classeListe.add(classe.getName_classe() + " (" + classe.getName_school() + ", " + classe.getMatiere() + ")");
                } while (c.moveToNext());
            }
        }

        c.close();
        return classeListe;
    }
}
