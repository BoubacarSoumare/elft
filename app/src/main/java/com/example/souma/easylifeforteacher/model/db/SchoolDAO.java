package com.example.souma.easylifeforteacher.model.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.souma.easylifeforteacher.MyApplication;
import com.example.souma.easylifeforteacher.model.beans.School;

import java.util.ArrayList;

/**
 * Created by souma on 8/23/2018.
 */

public class SchoolDAO {

    public static final String TABLE_NAME = "School";
    private static final String COL_INITIALS = "Initials";
    private static final String COL_NAME = "Name";
    public static final String COL_ID = "ID";

    public static final String SQL_CREATION_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_INITIALS + " TEXT,"
            + COL_NAME + " TEXT);";

    public static boolean insert(School school) {
        //On ouvre la base en ecriture
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        //Crétaion d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //On lui ajouter une valeur assoiée a une clé qui est le nom de la colonne
        values.put(COL_INITIALS, school.getInitials());
        values.put(COL_NAME, school.getName());

        school.setId(db.insert(TABLE_NAME, null, values));

        db.close();

        if (school.getId() <= 0) {
            //Gestion de l'erreur
            Log.w("TAG_SQL", "Insertion echoué");
            return false;
        }
        return true;
    }

    public static int update(School school) {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COL_INITIALS, school.getInitials());
        values.put(COL_NAME, school.getName());

        int nb = db.update(TABLE_NAME, values, COL_ID + " = " + school.getId(), null);
        db.close();
        return nb;
    }

    public static int deleteAll() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        int nb = db.delete(TABLE_NAME, null, null);
        db.close();
        return nb;
    }

    public static int deleteOne(School school) {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        int nb = db.delete(TABLE_NAME, COL_ID + " = " + school.getId(), null);
        db.close();
        return nb;
    }

    public static ArrayList<School> getSchools() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_INITIALS, COL_NAME}, null, null, null, null, null);
        ArrayList<School> arrayList = cursorToSchools(cursor);
        db.close();

        return arrayList;
    }

    /**
     * Une methode qui convertit un cursor en liste de schools
     */
    private static ArrayList<School> cursorToSchools(Cursor c) {
        ArrayList<School> schoolListe = new ArrayList<>();

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    String initials = c.getString(c.getColumnIndex(COL_INITIALS));
                    String name = c.getString(c.getColumnIndex(COL_NAME));
                    Long id = c.getLong(c.getColumnIndex(COL_ID));

                    School school = new School(initials, name);
                    school.setId(id);

                    schoolListe.add(school);
                } while (c.moveToNext());
            }
        }

        c.close();
        return schoolListe;
    }

    public static ArrayList<String> getStringSchools() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_INITIALS, COL_NAME}, null, null, null, null, null);
        ArrayList<String> arrayList = cursorToStringSchools(cursor);
        db.close();

        return arrayList;
    }

    private static ArrayList<String> cursorToStringSchools(Cursor c) {
        ArrayList<String> schoolListe = new ArrayList<>();

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    String initials = c.getString(c.getColumnIndex(COL_INITIALS));
                    String name = c.getString(c.getColumnIndex(COL_NAME));
                    Long id = c.getLong(c.getColumnIndex(COL_ID));

                    School school = new School(initials, name);
                    school.setId(id);

                    schoolListe.add(school.getInitials());
                } while (c.moveToNext());
            }
        }

        c.close();
        return schoolListe;
    }

    //retourne un cursor qu'on va utiliser dans le dialog
    public static Cursor getSchoolsCursor() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_INITIALS, COL_NAME}, null, null, null, null, null);
        db.close();

        return cursor;
    }
}
