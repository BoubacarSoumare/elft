package com.example.souma.easylifeforteacher.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.R;

public class SettingsActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    TextView tv_nb_hours, tv_before, tv_notify_me;
    SeekBar sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        tv_nb_hours = (TextView) findViewById(R.id.tv_nb_hours);
        tv_before = (TextView) findViewById(R.id.tv_before);
        tv_notify_me = (TextView) findViewById(R.id.tv_notify_me);
        sb = (SeekBar) findViewById(R.id.sb);

        sb.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress != 0) {
            tv_notify_me.setText(R.string.notify_me);
            tv_nb_hours.setText(" " + progress);
            if (progress == 1) {
                tv_before.setText(R.string.hour_before);
            } else {
                tv_before.setText(R.string.hours_before);
            }
        } else {
            tv_before.setText("");
            tv_nb_hours.setText("");
            tv_notify_me.setText(R.string.dont_notify_me);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (sb.getProgress() != 0) {
            if (sb.getProgress() == 1) {
                Toast.makeText(this, getString(R.string.you_will_be_notify) + " " + sb.getProgress() + " " + getString(R.string.hour_before), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.you_will_be_notify) + " " + sb.getProgress() + " " + getString(R.string.hours_before), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.you_will_not_be_notify, Toast.LENGTH_SHORT).show();
        }
    }
}
