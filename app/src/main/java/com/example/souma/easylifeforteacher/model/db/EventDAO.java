package com.example.souma.easylifeforteacher.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.souma.easylifeforteacher.MyApplication;
import com.example.souma.easylifeforteacher.model.beans.Event;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by souma on 8/25/2018.
 */

public class EventDAO {

    public static final String TABLE_NAME = "Event";
    private static final String COL_DATE = "Date";
    private static final String COL_DESCRIPTION = "Description";
    private static final String COL_NAME_CLASSE = "NameClasse";
    private static final String COL_NAME_MATIERE = "NameMATIERE";
    private static final String COL_NAME_SCHOOL = "NameSchool";
    public static final String COL_ID = "ID";

    public static final String SQL_CREATION_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_DATE + " INTEGER NOT NULL,"
            + COL_DESCRIPTION + " TEXT,"
            + COL_NAME_CLASSE + " TEXT,"
            + COL_NAME_MATIERE + " TEXT,"
            + COL_NAME_SCHOOL + " TEXT);";

    public static boolean insert(Event event) {
        //On ouvre la base en ecriture
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        //Crétaion d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //On lui ajouter une valeur assoiée a une clé qui est le nom de la colonne
        if (event.getDate() != null) {
            values.put(COL_DATE, event.getDate().getTime());

        }
        values.put(COL_DESCRIPTION, event.getDescription());
        values.put(COL_NAME_CLASSE, event.getName_classe());
        values.put(COL_NAME_MATIERE, event.getMatiere());
        values.put(COL_NAME_SCHOOL, event.getName_school());

        event.setId(db.insert(TABLE_NAME, null, values));

        db.close();

        if (event.getId() <= 0) {
            //Gestion de l'erreur
            Log.w("TAG_SQL", "Insertion echoué");
            return false;
        }
        return true;
    }

    /*
        public static int update(Classe classe) {
            SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(COL_CLASSE_NAME, classe.getName_classe());
            values.put(COL_MATIERE, classe.getMatiere());
            values.put(COL_ID_SCHOOL, classe.getId_school());
            values.put(COL_NAME_SCHOOL, classe.getName_school());

            int nb = db.update(TABLE_NAME, values, COL_ID + " = " + classe.getId(), null);
            db.close();
            return nb;
        }

        public static int deleteAll() {
            SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

            int nb = db.delete(TABLE_NAME, null, null);
            db.close();
            return nb;
        }
    */
    public static int deleteOne(Event event) {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        int nb = db.delete(TABLE_NAME, COL_ID + " = " + event.getId(), null);
        db.close();
        return nb;
    }

    public static ArrayList<Event> getEvents() {
        SQLiteDatabase db = MyApplication.getMySQLiteDatabase().getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_DATE, COL_DESCRIPTION, COL_NAME_CLASSE, COL_NAME_MATIERE, COL_NAME_SCHOOL}, null, null, null, null, null);
        ArrayList<Event> arrayList = cursorToEvents(cursor);
        db.close();

        return arrayList;
    }

    /**
     * Une methode qui convertit un cursor en liste de schools
     */
    private static ArrayList<Event> cursorToEvents(Cursor c) {
        ArrayList<Event> eventListe = new ArrayList<>();

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    Date date = new Date(c.getLong(c.getColumnIndex(COL_DATE)));
                    String description = c.getString(c.getColumnIndex(COL_DESCRIPTION));
                    String name_classe = c.getString(c.getColumnIndex(COL_NAME_CLASSE));
                    String name_matiere = c.getString(c.getColumnIndex(COL_NAME_MATIERE));
                    String name_school = c.getString(c.getColumnIndex(COL_NAME_SCHOOL));
                    Long id = c.getLong(c.getColumnIndex(COL_ID));

                    Event event = new Event(date, description, name_classe, name_matiere, name_school);
                    event.setId(id);

                    eventListe.add(event);
                } while (c.moveToNext());
            }
        }

        c.close();
        return eventListe;
    }
}
