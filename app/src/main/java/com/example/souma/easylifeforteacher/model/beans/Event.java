package com.example.souma.easylifeforteacher.model.beans;

import java.util.Date;

/**
 * Created by souma on 8/22/2018.
 */

public class Event {

    private Date date;
    private String description, name_school, name_classe, matiere;
    private Long id;
    private int id_school, id_classe;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName_school() {
        return name_school;
    }

    public void setName_school(String name_school) {
        this.name_school = name_school;
    }

    public String getName_classe() {
        return name_classe;
    }

    public void setName_classe(String name_classe) {
        this.name_classe = name_classe;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getId_school() {
        return id_school;
    }

    public void setId_school(int id_school) {
        this.id_school = id_school;
    }

    public int getId_classe() {
        return id_classe;
    }

    public void setId_classe(int id_classe) {
        this.id_classe = id_classe;
    }

    public Event(Date date, String description, String name_classe) {
        this.date = date;
        this.description = description;
        this.name_classe = name_classe;
    }

    @Override
    public String toString() {
        return "Event{" +
                "date=" + date +
                ", description='" + description + '\'' +
                '}';
    }

    public Event(Date date, String description, String name_classe, String matiere, String name_school) {
        this.date = date;
        this.description = description;
        this.name_school = name_school;
        this.name_classe = name_classe;
        this.matiere = matiere;
    }
}
