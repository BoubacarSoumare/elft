package com.example.souma.easylifeforteacher.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.souma.easylifeforteacher.NotificationUtils;
import com.example.souma.easylifeforteacher.R;
import com.example.souma.easylifeforteacher.model.beans.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by souma on 8/21/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private static boolean ok = true;


    private static final SimpleDateFormat formaten = new SimpleDateFormat("EEE, MMM d, '' yy  hh: mm a");
    private static final SimpleDateFormat formatfr = new SimpleDateFormat("EEE, d MMM, yyyy  hh: mm");
    private static final SimpleDateFormat formatfr24 = new SimpleDateFormat("EEE, d MMM, yyyy  HH: mm");

    private ArrayList<Event> events;
    private OnEventListener onEventListener;

    public EventAdapter(ArrayList<Event> events, OnEventListener onEventListener) {
        this.events = events;
        this.onEventListener = onEventListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_event, parent, false);

        return new EventAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Event event = events.get(position);

        if (Locale.getDefault().getLanguage().equals("en")) {
            holder.tv_event_date.setText(formaten.format(event.getDate()));
        } else if (Locale.getDefault().getLanguage().equals("fr")) {
            if (!DateFormat.is24HourFormat(holder.tv_event_description.getContext())) {
                holder.tv_event_date.setText(formatfr.format(event.getDate()));
            } else {
                holder.tv_event_date.setText(formatfr24.format(event.getDate()));
            }
        }
        holder.tv_event_description.setText(event.getDescription());
        //CountDown
        holder.tv_event_school.setText(getCountdownText(holder.tv_event_school.getContext(), event.getDate()));
        startCountdown(holder, event);
        //notify
        notifyTime(holder.tv_event_school.getContext(), event.getDate());

        holder.tv_event_classe.setText(event.getName_classe());
        holder.tv_event_matiere.setText(event.getMatiere());

        holder.bt_options_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventListener != null) {
                    onEventListener.onclick(event);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_event_date, tv_event_description, tv_event_school, tv_event_classe, tv_event_matiere;
        public View bt_options_event;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_event_date = (TextView) itemView.findViewById(R.id.tv_event_date);
            tv_event_description = (TextView) itemView.findViewById(R.id.tv_event_description);
            tv_event_school = (TextView) itemView.findViewById(R.id.tv_event_school);
            tv_event_classe = (TextView) itemView.findViewById(R.id.tv_event_classe);
            tv_event_matiere = (TextView) itemView.findViewById(R.id.tv_event_matiere);
            bt_options_event = itemView.findViewById(R.id.bt_options_event);
        }
    }

    // Notre interface de communication pour le callback
    public interface OnEventListener {
        void onclick(Event event);
    }

    /**
     * Returns a formatted string containing the amount of time (days, hours,
     * minutes, seconds) between the current time and the specified future date.
     *
     * @param context
     * @param futureDate
     * @return
     */
    public static CharSequence getCountdownText(Context context, Date futureDate) {
        StringBuilder countdownText = new StringBuilder();

        // Calculate the time between now and the future date.
        long timeRemaining = futureDate.getTime() - new Date().getTime();

        // If there is no time between (ie. the date is now or in the past), do nothing
        if (timeRemaining > 0) {
            Resources resources = context.getResources();

            // Calculate the days/hours/minutes/seconds within the time difference.
            //
            // It's important to subtract the value from the total time remaining after each is calculated.
            // For example, if we didn't do this and the time was 25 hours from now,
            // we would get `1 day, 25 hours`.
            int days = (int) TimeUnit.MILLISECONDS.toDays(timeRemaining);
            timeRemaining -= TimeUnit.DAYS.toMillis(days);
            int hours = (int) TimeUnit.MILLISECONDS.toHours(timeRemaining);
            timeRemaining -= TimeUnit.HOURS.toMillis(hours);
            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(timeRemaining);
            timeRemaining -= TimeUnit.MINUTES.toMillis(minutes);
            int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(timeRemaining);

            // For each time unit, add the quantity string to the output, with a space.
            if (days > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.days, days, days));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.hours, hours, hours));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0 || minutes > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.minutes, minutes, minutes));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0 || minutes > 0 || seconds > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.seconds, seconds, seconds));
                countdownText.append(" ");
            }
        }

        return countdownText.toString();
    }

    /**
     * The time (in ms) interval to update the countdown TextView.
     */
    private static final int COUNTDOWN_UPDATE_INTERVAL = 500;

    private Handler countdownHandler;

    /**
     * Stops the  countdown timer.
     */
    /*private void stopCountdown() {
        if (countdownHandler != null) {
            countdownHandler.removeCallbacks(updateCountdown);
            countdownHandler = null;
        }
    }*/

    /**
     * (Optionally stops) and starts the countdown timer.
     */
    void startCountdown(ViewHolder hol, Event event) {
        //stopCountdown();
        countdownHandler = new Handler();
        maj(hol, event);
    }

    void maj(final ViewHolder hol, final Event event) {
        /**
         * Updates the countdown.
         */
        final Runnable updateCountdown = new Runnable() {
            @Override
            public void run() {
                try {
                    hol.tv_event_school.setText(getCountdownText(hol.tv_event_school.getContext(), event.getDate()));
                } finally {
                    countdownHandler.postDelayed(this, COUNTDOWN_UPDATE_INTERVAL);
                    if (EventAdapter.ok) {
                        if ((event.getDate().getTime() - new Date().getTime()) <= 3600000) {
                            NotificationUtils.sendNotification(hol.tv_event_school.getContext(), hol.tv_event_school.getContext().getResources().getString(R.string.notification_message1) + " " + event.getDescription() + " " + hol.tv_event_school.getContext().getResources().getString(R.string.notification_message2) + " " + event.getName_classe());
                            //NotificationUtils.programmeNotification(hol.tv_event_school.getContext(), "Vous avez un " + event.getDescription(), 1000);
                            ok = false;
                        }
                    }
                }
            }
        };
        updateCountdown.run();
    }

    public void clearAll() {
        countdownHandler.removeCallbacksAndMessages(null);
    }

    //Notifications
    public static void notifyTime(Context context, Date futureDate) {

        // Calculate the time between now and the future date.
        long timeRemaining = futureDate.getTime() - new Date().getTime();
        long seconds = timeRemaining / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        if (timeRemaining == 3600000) {
            //NotificationUtils.sendNotification(holder.tv_event_school.getContext(), "Vous avez un evenement");
            NotificationUtils.programmeNotification(context, "Vous avez un evenement", 1000);
        }

    }
}
