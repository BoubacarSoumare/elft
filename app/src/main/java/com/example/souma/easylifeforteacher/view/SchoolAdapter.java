package com.example.souma.easylifeforteacher.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.souma.easylifeforteacher.R;
import com.example.souma.easylifeforteacher.model.beans.School;

import java.util.ArrayList;

/**
 * Created by souma on 8/21/2018.
 */

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.ViewHolder> {

    private ArrayList<School> schools;
    private OnSchoolListener onSchoolListener;

    public SchoolAdapter(ArrayList<School> schools, OnSchoolListener onSchoolListener) {
        this.schools = schools;
        this.onSchoolListener = onSchoolListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_school, parent, false);

        return new SchoolAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final School school = schools.get(position);

        holder.tv_initials.setText(school.getInitials().toUpperCase());
        holder.tv_name_school.setText(school.getName());

        holder.bt_options_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSchoolListener != null) {
                    onSchoolListener.onclick(school);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_initials, tv_name_school;
        public View bt_options_school;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_initials = (TextView) itemView.findViewById(R.id.tv_initials);
            tv_name_school = (TextView) itemView.findViewById(R.id.tv_name_school);
            bt_options_school = itemView.findViewById(R.id.bt_options_school);
        }
    }

    // Notre interface de communication pour le callback
    public interface OnSchoolListener {
        void onclick(School school);
    }
}
