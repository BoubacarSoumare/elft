package com.example.souma.easylifeforteacher.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.souma.easylifeforteacher.R;
import com.example.souma.easylifeforteacher.SharedPreferencesUtils;
import com.example.souma.easylifeforteacher.model.beans.User;

public class InscriptionActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_username, et_password, et_password_confirm;
    Button bt_inscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_password_confirm = (EditText) findViewById(R.id.et_password_confirm);
        bt_inscription = (Button) findViewById(R.id.bt_inscription);

        bt_inscription.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (et_username.length() == 0 || et_password.length() == 0) {
            Toast.makeText(this, "champs vide non autorisé", Toast.LENGTH_SHORT).show();
        } else {
            if (!et_password.getText().toString().equals(et_password_confirm.getText().toString())) {
                Toast.makeText(this, "la confirmation doit etre identique au mot de passe", Toast.LENGTH_SHORT).show();
            } else {
                User user = new User(et_username.getText().toString(), et_password.getText().toString());
                SharedPreferencesUtils.saveUser(this, user);
                Toast.makeText(this, "Utilisateur enregistré", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(InscriptionActivity.this, ConnectionActivity.class);
                startActivity(intent);
            }
        }
    }
}
